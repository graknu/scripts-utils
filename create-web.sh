#!/bin/bash
#
# create-web.sh
#

USER=$1
TLD_SERVER="6admin.net"


[ -z $FQDN_USER ] && FQDN_USER="${USER}.${TLD_SERVER}"
[ -z $MAIL_USER ] && MAIL_USER="noc@${TLD_SERVER}"
[ -z $DEBUG ] && DEBUG=0

CONF_DIR="/etc/webmanage/"

if [ ! -d $CONF_DIR ]
then
	echo "==> Creation du dossier de configuration"
	mkdir -p $CONF_DIR
	touch $CONF_DIR/vhost.conf
fi


if [ -z $USER ]
then
	echo "==> Merci de saisir un utilisateur"
	exit 1
fi

TEST_USER=$(grep "^${USER}:" /etc/passwd)
if [ -z $TEST_USER ]
then
	PASSWD="#${USER}#"
	HOME_DIR="/home/${USER}"
	WEB_DIR="${HOME_DIR}/www"
	BASE_GROUP="webusers"

	TEST_GROUP_WEBUSERS=$(grep "^${BASE_GROUP}:" /etc/group)
	if [ -z $TEST_GROUP_WEBUSERS ]
	then
		groupadd $BASE_GROUP
	fi

	if [ $DEBUG == 1 ]
	then
		echo "User : ${USER}"
		echo "Password : ${PASSWD}"
		echo "Home Dir : ${HOME_DIR}"
		echo "Web Dir : ${WEB_DIR}"
		echo "Group base : ${BASE_GROUP}"
	fi

	useradd -d ${HOME_DIR} -m -G ${BASE_GROUP} -s /bin/nologin ${USER}
	if [ $? == 0 ]
	then
		echo "==> Ajout du mot de passe pour l'utilisateur"
		echo "${USER}:${PASSWD}" | chpasswd
		echo "==> Creation du dossier WWW"
		mkdir ${WEB_DIR}
		echo "==> Mise en droit sur le dossier www"
		chown ${USER}:${USER} ${WEB_DIR}
		chown root:root ${HOME_DIR}
		#chmod 770 -R ${WEB_DIR}

		TEST_USER_CONF_WEBMANAGE=$(grep "^${USER}:" $CONF_DIR/vhost.conf)
		VHOST_NAME="vhost_${USER}.conf"
		APACHE_CONF_SITE_AVAILABLE="/etc/apache2/sites-available"
		APACHE_LOG_DIR="/var/log/apache2/${USER}"

		if [ -z $TEST_USER_CONF_WEBMANAGE ]
		then
			echo "==> Ajout de l'utilisateur dans la configuration vhost.conf"
			echo "${USER}:${VHOST_NAME}" >> $CONF_DIR/vhost.conf

			echo "==> Création du vhost pour l'utilisateur"
			cp -v $CONF_DIR/vhost.template $APACHE_CONF_SITE_AVAILABLE/$VHOST_NAME
			if [ $? == 0 ]
			then
				if [ $DEBUG == 1 ]
				then
					echo "Domain : ${FQDN_USER}"
					echo "Mail : ${MAIL_USER}"
				fi

				echo "==> Application de la configuration vhost"
				sed -e "s/##DOMAIN##/${FQDN_USER}/g" -i $APACHE_CONF_SITE_AVAILABLE/$VHOST_NAME
				sed -e "s/##ADMINMAIL##/${MAIL_USER}/g" -i $APACHE_CONF_SITE_AVAILABLE/$VHOST_NAME
				sed -e "s/##USER##/${USER}/g" -i $APACHE_CONF_SITE_AVAILABLE/$VHOST_NAME

				if [ ! -d $APACHE_LOG_DIR ]
				then
					mkdir -p $APACHE_LOG_DIR
					chown www-data:www-data $APACHE_LOG_DIR
				fi

				a2ensite $VHOST_NAME 2>&1 > /dev/null
				if [ $? == 0 ]
				then
					systemctl restart apache2.service
				fi
			fi
		else
			echo "==> L'utlisateur est deja present dans la configuration vhost.conf"
		fi
	fi
else
	echo "==> Utilisateur deja existant"
fi

