#!/bin/bash
#
# Permet de mettre en place les guest QEMU
#

if [ $UID != 0 ]
then
	echo "==> Merci d'executer ce script en root"
	exit 1
fi

OS_RELEASE=$(cat /etc/os-release | grep -i "^ID=" | cut -d "=" -f 2)
TEST_QEMUGA=$(qemu-ga --version | head -n 1)

case $OS_RELEASE in
	"debian")
		if [ -z "${TEST_QEMUGA}" ]
		then
			echo "==> Installation de QEMU-GUEST-AGENT"
			apt install qemu-guest-agent apt-transport-https wget gnupg2 curl
			systemctl enable qemu-guest-agent.service
			if [ $? == 0 ]
			then
				echo "Le redémarrage système est obligatoire !"
			fi

			wget -qO- https://repos.influxdata.com/influxdb.key | apt-key add -
			echo "deb https://repos.influxdata.com/debian buster stable" | tee /etc/apt/sources.list.d/influxdb.list
			
			apt update
			if [ $? == 0 ]
			then
				apt install telegraf
			fi

		fi
	;;
	*)
		echo "==> Distribution non prise en charge !"
	;;
esac
